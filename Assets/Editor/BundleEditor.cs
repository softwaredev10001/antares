﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BundleEditor : Editor
{
    [MenuItem("Assets/ Build AssetsBundle")]
    // Start is called before the first frame update
    static void BuildAllAssetsBundles()
    {
        BuildPipeline.BuildAssetBundles(@"C:\Users\Peter\Desktop\unity-test\Antares_V1\Assets\StreamingAssets", BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);
    }
}
