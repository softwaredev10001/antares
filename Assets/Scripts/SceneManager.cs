﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

/// <summary>
/// Manager which manages the scene for the Antares Test
/// </summary>
public class SceneManager : MonoBehaviour
{ 

    public struct NPCData
    {
        internal int ActorId;
        internal char Ethnicity;
        internal string audioFile;
    }

    public struct EventData
    {
        internal int EventId;
        internal string CurrObjName;
        internal string CurrCorrectSolution;
        internal DateTime CurrTime;

        public override string ToString()
        {
            string result = "";
            result += EventId + ",";
            result += CurrObjName + ",";
            result += CurrCorrectSolution;
            return result;
        }
    }

    public struct InteractionData
    {
        internal string InteractionType;
        internal string Solution;
        internal string Direction;
        internal DateTime CurrTime;
        internal int ElapsedTime;

        public override string ToString()
        {
            string result = "";
            result += InteractionType + ",";
            result += Direction + ",";
            result += Solution + ",";
            result += ElapsedTime.ToString();
            return result;
        }
    }

    public GameObject[] AvatarPlaceholders;

    private System.Random Randomizer;
    private FileWriter Writer;
    public GameObject Placeholder;
    private DateTime StartTime;
    private DateTime ActorStartTime;
    internal Queue<EventData> Specification;
    internal Queue<NPCData> ActorSpecification;
    private EventData CurrEvent;
    private NPCData CurrActorEvent;
    private Vector3 OriginPosition;
    private AssetBundle FruitAssets;
    private AssetBundle Drugs;

    void Start()
    {
        // IMPORTANT: Due to the nature of Unitys design philosophy, YOU SHOULD NEVER EVER initalize the Queue inside the Start()-methode
        Debug.Log("SceneManager->Start()");
        Writer = GetComponent<FileWriter>();
        OriginPosition = GameObject.Find("Placeholder").transform.position;
        Randomizer = new System.Random();
    }

    private void ShuffleAvatarPositions()
    {
        Debug.Log("SceneManager->ShuffleAvatarPositions()");
        for(int currPlaceholder = 0;currPlaceholder < AvatarPlaceholders.Length;currPlaceholder++)
        {
            int randomIndex = Randomizer.Next(0,AvatarPlaceholders.Length);
            GameObject buffer = AvatarPlaceholders[currPlaceholder];
            AvatarPlaceholders[currPlaceholder] = AvatarPlaceholders[randomIndex];
            AvatarPlaceholders[randomIndex] = buffer;
        }
    }

    private void LoadModelFromAssetBundle(string AssetName)
    {
        Debug.Log("Current object: " + AssetName);
        if (FruitAssets == null)
        {
            FruitAssets = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "fruits"));
        }

        GameObject prefab = FruitAssets.LoadAsset<GameObject>(AssetName);

        if(prefab == null)
        {
            Debug.Log("Error loading model ...");
        }

        prefab.name = "ActiveObject";
        prefab.transform.position = GameObject.Find("Placeholder").transform.position;
        prefab.AddComponent<Interaction>();
        if(prefab.GetComponent<Rigidbody>() == null)
            prefab.AddComponent<Rigidbody>();
        if(prefab.GetComponent<BoxCollider>() == null)
            prefab.AddComponent<BoxCollider>();
        prefab.GetComponent<BoxCollider>().center = new Vector3(-3.227229e-15f, 0.07763499f, -0.001521677f);
        prefab.GetComponent<BoxCollider>().size = new Vector3(0.1f, 0.1764439f, 0.1521301f);
        prefab.transform.rotation = Quaternion.Euler(0, 0, 0);
        Instantiate(prefab);
    }

    private void PrepareNextEvent()
    {
        Debug.Log("PrepareNextEvent->QueueSize[Before]:"+Specification.Count);
        if (Specification.Count >= 1){
            CurrEvent = Specification.Dequeue();
            LoadModelFromAssetBundle(CurrEvent.CurrObjName);
        }
        StartTime = DateTime.Now;
        Debug.Log("PrepareNextEvent->QueueSize[After]:" + Specification.Count);
    }

    private void PrepareNextActorEvent()
    {
        Debug.Log("PrepareActorEvent()->QueueSize[Before]:" + ActorSpecification.Count);
        if(ActorSpecification.Count >= 1){
            CurrActorEvent = ActorSpecification.Dequeue();
        }
        ActorStartTime = DateTime.Now;
        Debug.Log("PrepareActorEvent->QueueSize[After]:" + ActorSpecification.Count);
    }

    internal void HandleUserInteraction(InteractionData interaction)
    {
        interaction.ElapsedTime = (int) Math.Round(interaction.CurrTime.Subtract(StartTime).TotalSeconds);
        interaction.Solution = CurrEvent.CurrCorrectSolution;
        GameObject.DestroyImmediate(GameObject.Find("ActiveObject(Clone)"));
        Writer.GetInteractionData(interaction);
        PrepareNextEvent();
    }

    /// <summary>
    /// Converts the events from the CSV file into events used by the SceneManager
    /// </summary>
    /// <param name="Events">Events which originate from the chosen csv file</param>
    internal void ReceiveEventData(Queue<EventData> Events)
    {
        Specification = Events;
        PrepareNextEvent();
    }

    internal void ReceiveNPCData(Queue<NPCData> data)
    {
        ActorSpecification = data;
        PrepareNextActorEvent();
    }
}
