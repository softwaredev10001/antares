﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour
{
    private System.Random RandomizerObj;
    private List<Transform> CList;

    // Shuffle the actors list to generate a pseudo-random constellation
    internal List<GameObject> ShuffleActors(List<GameObject> Input)
    {
        RandomizerObj = new System.Random();
        int n = Input.Count;
        while( n > 1 ){
            n--;
            int k = RandomizerObj.Next(n + 1);
            GameObject value = Input[k];
            Input[k] = Input[n];
            Input[n] = value;
        }
        return Input;
    }

    internal void AssignRandomizedPositions(List<GameObject> Actors)
    {
        int currChairIndex = 0;
        CList = new List<Transform>();
        Transform ChairList = transform.GetChild(1);

        foreach(Transform currChair in ChairList)
        {
            CList.Add(currChair);
        }

        foreach(GameObject child in Actors)
        {
            Debug.Log(child.transform.position);
            currChairIndex = RandomizerObj.Next(0, CList.Count);
            Transform currChair = CList[currChairIndex];

            // Set a new position for the avatar
            Vector3 avatarPos = new Vector3(
                currChair.position.x - 0.126f,
                currChair.position.y-0.5f,
                currChair.position.z
            );

            // Adapt avatar's rotation to the rotation of the chair
            Vector3 avatarRot = new Vector3(
                0.0f,
                currChair.eulerAngles.y,
                0.0f
            );

            // Set new avatar position and rotation
            child.transform.position = avatarPos;
            child.transform.eulerAngles = avatarRot;
            CList.Remove(currChair);
        }
    }
}
