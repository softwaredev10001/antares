﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileHandler : MonoBehaviour
{
    private int Stage;
    private string InputFolder;
    private string OutputFolder;
    private string[] ToReadContent;
    private string ToWriteContent;
    private SceneManager sceneManager;

    // Start is called before the first frame update
    void Start()
    {
        Stage = 0;
        InputFolder = "Assets/Configurations/";
        OutputFolder = "/Results";
        sceneManager = GetComponent<SceneManager>();
        ReadFile("FruitsOnly");
    }

    void ReadFile(string name)
    {
        Queue<string> result = new Queue<string>();
        try
        {
            using (StreamReader reader = new StreamReader(InputFolder + name + ".csv"))
            {
                while(reader.Peek() > 0)
                {
                    result.Enqueue(reader.ReadLine());
                }
            }
            // sceneManager.HandleMessage(0,result);
        }catch(IOException e)
        {
            Debug.Log(e);
        }
    }

    void WriteFile(string name,string content)
    {
        using (StreamWriter writer = new StreamWriter(OutputFolder + name + ".csv"))
        {
            try
            {
                writer.Write(content);
            }catch(IOException e)
            {
                Debug.Log("Exception occured:" + e.Message);
            }
        }
    }
}
