﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    private Vector3 InitialPosition;
    private bool ObjectMoved;
    private char Direction;
    private SceneManager sceneManager;

    void Start()
    {
        InitialPosition = gameObject.transform.position;
        ObjectMoved = false;
        Direction = '?';
        sceneManager = GameObject.Find("World").GetComponent<SceneManager>();
    }

    private void GetMovementDirection()
    {
        Vector3 currPosition = gameObject.transform.position;

        if(currPosition.z < InitialPosition.z-0.25f){
            Direction = 'L';
            ObjectMoved = true;
        }else if(currPosition.z > InitialPosition.z+0.25f){
            Direction = 'R';
            ObjectMoved = true;
        }else if(currPosition.x < InitialPosition.x-0.25f){
            Direction = 'F';
            ObjectMoved = true;
        }else if(currPosition.x > InitialPosition.x +0.25f){
            Direction = 'B';
            ObjectMoved = true;
        }

        if (ObjectMoved){
            SceneManager.InteractionData data = ConvertInteractionToInteractionData("Moved");
            GameObject.Find("World").GetComponent<SceneManager>().HandleUserInteraction(data);
        }
    }

    /// <summary>
    /// Convert the user interaction into the InteractionData format
    /// </summary>
    /// <param name="Interaction">Type of interaction</param>
    /// <param name="Partner">Interaction partner (object which collided with the controller)</param>
    /// <returns></returns>
    private SceneManager.InteractionData ConvertInteractionToInteractionData(string Interaction)
    {
        SceneManager.InteractionData result = new SceneManager.InteractionData();
        result.CurrTime = System.DateTime.Now;
        result.InteractionType = Interaction;
        if (Direction == 'L'){
            result.Direction = "Left";
        }else if (Direction == 'R'){
            result.Direction = "Right";
        }else if(Direction == 'B'){
            result.Direction = "Backwards";
        }else if(Direction == 'F'){
            result.Direction = "Forward";
        }
        return result;
    }

    void Update()
    {
        if (!ObjectMoved){
            GetMovementDirection();
        }
    }
}
