﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Actors : MonoBehaviour
{
    private bool FirstEvent;
    private bool EventQueueNotEmpty;
    private bool FocusedAudioSourceInitalized;
    private bool FocusedAudioSourceIsPlaying;
    private bool IsPausing;
    private bool PauseFinished;
    private bool Started;

    private Queue<SceneManager.NPCData> IncomingEvents;
    private int IncomingEventsLength;
    private List<GameObject> ActorList;
    private AudioClip selectedClip;
    private Animator FocusedAnimator;
    private AudioSource FocusedAudioSource;
    private System.Random Randomizer;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Actors()->Start()");
        // Initalize all controll values
        EventQueueNotEmpty = false;
        FirstEvent = false;
        FocusedAudioSourceInitalized = false;
        FocusedAudioSourceIsPlaying = false;
        IsPausing = false;
        PauseFinished = false;
        Started = true;

        // Initalize member variables of the script
        ActorList = new List<GameObject>();
        Randomizer = new System.Random();
        IncomingEvents = null;
        IncomingEventsLength = 0;

   
     // Get all files from the directory "Assets/Resources/Audio"
        DirectoryInfo clipsDir = new DirectoryInfo("Assets/Resources/Audio");
        FileInfo[] files = clipsDir.GetFiles();

        // Iterate trough all files inside the "Assets/Resources/Audio" folder
        foreach (FileInfo file in files)
        {
            // Check whether file is an mp3 file and doesn't contain the meta extension
            bool isMp3 = file.Name.EndsWith("wav");
            bool isNotMeta = !(file.Name.Contains("meta"));
            if (isMp3 && isNotMeta)
            {
                // Load the audio file and add it to the list of audio files
                string processedFileName = file.Name.Replace(".wav", "");
                AudioClip ToAddClip = Resources.Load<AudioClip>("Audio/" + processedFileName);
            }
        }
        // Reference all actors inside the scene
        foreach (Transform child in transform.GetChild(2))
        {
            if (child.gameObject.tag == "actor") {
                ActorList.Add(child.gameObject);
            }
        }

        Debug.Log("ActorList before Shuffle:");
        string output = "[";
        foreach(GameObject g in ActorList){
            if(ActorList.IndexOf(g) == ActorList.Count-1){
                output += g.name + "]";
            }else{
                output += g.name + ",";
            }
        }
        Debug.Log(output);

        ActorList = gameObject.GetComponent<Randomizer>().ShuffleActors(ActorList);

        Debug.Log("Actor list after shuffle");
        output = "[";
        foreach (GameObject g in ActorList)
        {
            if (ActorList.IndexOf(g) == ActorList.Count - 1)
            {
                output += g.name + "]";
            }
            else
            {
                output += g.name + ",";
            }
        }

        GetComponent<Randomizer>().AssignRandomizedPositions(ActorList);
        Debug.Log(output);
    }

    // Update is called once per frame
    void Update()
    {
        // Initalize all possible events
        FirstEvent = IncomingEvents.Count == IncomingEventsLength;
        EventQueueNotEmpty = IncomingEvents.Count > 0;
        FocusedAudioSourceInitalized = FocusedAudioSource != null;

        // If the AudioSource is initalized and its Playing, the AudioSource is playing
        if(FocusedAudioSourceInitalized){
            FocusedAudioSourceIsPlaying = FocusedAudioSourceInitalized && FocusedAudioSource.isPlaying;
        }

        if(IsPausing){
            return;
        }

        // Only animate, if the event queue is not empty !
        if (EventQueueNotEmpty)
        {
            // If then AudioSource is NOT playing, we can process a new event 
            if (!FocusedAudioSourceIsPlaying )
            {
                if (FirstEvent)
                {
                    if (!IsPausing && !PauseFinished)
                    {
                        StartCoroutine("EventDelay");
                    }
                    else if (!IsPausing && PauseFinished)
                    {
                        ProcessEvent(IncomingEvents.Dequeue());
                    }
                }
                else {
                    Debug.Log("Processing new event ...");
                    CancelEventProcessing();
                    if (!IsPausing && !PauseFinished)
                    {
                        StartCoroutine("EventDelay");
                    }
                    else if (!IsPausing && PauseFinished)
                    {
                        ProcessEvent(IncomingEvents.Dequeue());
                    }
                }
            }
        } else if (!EventQueueNotEmpty) {
            if (!FocusedAudioSourceIsPlaying)
            {
                CancelEventProcessing();
                return;
            }
        }
    }

    public void ProcessAllEvents()
    {
    }

    IEnumerator EventDelay()
    {
        Debug.Log("Waiting on delay ...");
        IsPausing = true;
        yield return new WaitForSeconds(3);
        IsPausing = false;
        PauseFinished = true;
        Debug.Log("Delay finished!");
    }

    private void CancelEventProcessing()
    {
        FocusedAudioSource.clip = null;
        FocusedAudioSource.Stop();
        FocusedAnimator.SetBool("IsFocused", false);
        FocusedAnimator.SetBool("IsIdle", true);
        return;
    }

    // Process an event which is passed as a string
    private void ProcessEvent(SceneManager.NPCData processedEvent)
    {
        Started = false;
        PauseFinished = false;
        Debug.Log("Actors -> ProcessEvent(e) -> Start");
        GameObject actor = ActorList[((processedEvent.Ethnicity) % 64) - 1];
        Debug.Log("Current actor: " + actor.name);
        FocusedAnimator = actor.gameObject.GetComponent<Animator>();
        Debug.Log("Computed index: "+ActorList[0].name);
        FocusedAudioSource = actor.gameObject.GetComponent<AudioSource>();
        FocusedAudioSource.clip = Resources.Load<AudioClip>("Audio/"+processedEvent.audioFile) as AudioClip;
        Debug.Log("Now playing : " + FocusedAudioSource.clip);
        FocusedAudioSource.Play();
        FocusedAnimator.SetBool("IsFocused", true);
        FocusedAnimator.SetBool("IsIdle", false);
    }

    // Receive event queue from other script file
    internal void ReceiveEventQueues(Queue<SceneManager.NPCData> events)
    {
        IncomingEvents = events;
        IncomingEventsLength = IncomingEvents.Count;
    }
}
