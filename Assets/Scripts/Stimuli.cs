﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stimuli : MonoBehaviour
{
    private Rigidbody Rigidbody;
    private bool IsHeld;
    private Transform Parent;
    private Transform Category;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        Parent = transform.parent;
        IsHeld = false;
    }

    /**
     * If an stimulus collides with a box,the scene manager is notified about the decision of the participant by calling
     * a static method from its class definition
     */
    void OnTriggerEnter(Collider other)
    {
        Queue<string> result = new Queue<string>();
        result.Enqueue(other.name);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
