﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileWriter : MonoBehaviour
{

    private SceneManager manager;
    private Queue<string> ToWriteEvents;
    private string content;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("FileWriter -> Start() -> START");
        manager = GetComponent<SceneManager>();
        Debug.Log("FileWriter -> End() -> End");
    }

    internal void GetInteractionData(SceneManager.InteractionData data)
    {
        content = data.ToString() + "\n";
        Debug.Log("FileWriter -> " + content);
        WriteFileStream(data.CurrTime.ToString()+".csv");
    }

    private bool WriteFileStream(string file)
    {
        Debug.Log("FileWriter -> WriteFileStream()");
        string FilePath = Application.dataPath + "/Results/result.csv";

        if (File.Exists(FilePath)){
            File.AppendAllText(FilePath,content);
        }else if (!File.Exists(FilePath)){
            using (StreamWriter StreamWriter = new StreamWriter(FilePath))
            {
                StreamWriter.Write(content);
            }
        }

        Debug.Log("FileWriter -> WriterFileStream()");
        return true;
    }
}
