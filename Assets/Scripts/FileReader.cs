﻿using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileReader : MonoBehaviour
{
    private SceneManager manager;
    private string[] ActorContent;
    private string[] Content;
    private Queue<SceneManager.EventData> Events;
    private Queue<SceneManager.NPCData> ActorEvents;
    private List<Vector3> Positions;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("FileReader->Start()->Begin");
        manager = GetComponent<SceneManager>();
        Content = new string[2];
        Events = new Queue<SceneManager.EventData>();
        SendEventsToLab("FruitsDemo.csv");
        Debug.Log("FileReader->End()->End");
    }

    private SceneManager.EventData ConvertToEventData(string[] Input)
    {
        SceneManager.EventData result = new SceneManager.EventData();
        result.EventId = Int32.Parse(Input[0]);
        result.CurrObjName = Input[1];
        result.CurrCorrectSolution = Input[2];
        result.CurrTime = DateTime.Now;
        return result;
    }

    /// <summary>
    /// Converts the content of the actor specification file into a corresponding NPCData object
    /// </summary>
    /// <param name="Input">Input retrieved from the file</param>
    /// <returns>Converted NPCData object</returns>
    private SceneManager.NPCData ConvertToNPCData(string[] Input)
    {
        SceneManager.NPCData result = new SceneManager.NPCData();
        result.ActorId = 0;
        result.Ethnicity = Char.Parse(Input[0]);
        result.audioFile = Input[1];
        return result;
    }

    /// <summary>
    /// Read file with the specified name
    /// </summary>
    /// <param name="file">Filename</param>
    /// <returns>Boolean value whether the file was read successfully</returns>
    private bool ReadFileStream(string file)
    {
        Debug.Log("FileReader->ReadFileStream()->Begin");
    
        if(!file.Contains(".csv")){
            Debug.LogError("Specified file has to be a CSV file!");
            return false;
        }

        // Read the specified file and generate its content
        using (StreamReader FileReader = new StreamReader(Application.dataPath + "/Configurations/" + file))
        {
            Content = FileReader.ReadToEnd().Split('\n');
        }

        // Enqueue the content to the EventQueue
        for (int currEvent = 0;currEvent < Content.Length;currEvent++)
        {
            string[] CurrEventSpec = Content[currEvent].Split(',');
            SceneManager.EventData data = ConvertToEventData(CurrEventSpec);
            Events.Enqueue(data);
        }

        Debug.Log("FileReader->ReadFileStream()->End");
        return true;
    }

    private bool ReadActorFileStream()
    {
        Debug.Log("FileReader -> ReadActorFileStream(filePath)");

        using (StreamReader FileReader = new StreamReader(Application.dataPath + "/Configurations/ActorsDemo.csv" ))
        {
            ActorContent = FileReader.ReadToEnd().Split('\n');
        }

        for(int currEvent = 0; currEvent < ActorContent.Length;currEvent++)
        {
            string currSpec = ActorContent[currEvent];
            string[] Npc_Data_Spec = currSpec.Split(',');
            SceneManager.NPCData data = ConvertToNPCData(Npc_Data_Spec);
            ActorEvents.Enqueue(data);
        }

        return true;
    }

    /// <summary>
    /// Sends the events to the SceneManager
    /// </summary>
    /// <param name="file"></param>
    public void SendEventsToLab(string file)
    {   
        ReadFileStream(file);
        GetComponent<SceneManager>().ReceiveEventData(Events);
    }

    /// <summary>
    /// Sends the events to the actors
    /// </summary>
    /// <param name="file"></param>
    public void SendEventsToActors(string file)
    {
        ReadFileStream(file);
        // gameObject.GetComponent<Actors>().ReceiveEventQueues(ToPassEvents);
    }
}
