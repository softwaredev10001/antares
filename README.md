# Notes on contributing
## Comment your code
Please comment your code, even if your function/class/module is not completed. It helps to understand how your code works. This is especially useful for emergency cases 
when multiple developers are working on the same stuff.
## Choose representative literals
Another best practice are representative literals for every variable, function and class. If you name your class **VR_Controller_Handler** instead of **Control_Handler**, 
the context of the class is explicity defined.
## Discuss merge conflicts as soon as possible
Don't try to resolve merge conflicts by simply mergin your version of the corresponding file. Whenever you can, send the latest file author a mail. Otherwise changes by other developers
may be lost.
## Use the wiki
If you don't understand anything inside the source code, look up the wiki! It contains everything from the architecture to the dataflow of the application.  